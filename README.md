# Run code on a machine with docker

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker build -t time-tracking-django-api .
```

2. run a container

```bash
docker run --rm -p 8080:80 time-tracking-django-api
```

# Swagger Docs

Please check the endpoints docs from `localhost:8080/docs/`. For using endpoints, followe this senario:
1. Create a new user by `v1/auth/register/` endpoint
2. Get the new user `access` by `v1/auth/login/` endpoint
3. Login by adding this `access` through `Authorize` button
4. Create a project through `post` method `/v1/api/project/`
5. Get all projects through `get` method `/v1/api/project/`. Get One of projects `id`
6. Create a time log throut `post` method `/v1/api/time_log/` using project `id` from previous step
7. Get all time logs through `get` method `/v1/api/time_log/`. Get One of time logs `id`
8. Update time log throug `put` method `/v1/api/time_log/{id}/` using time log `id` from previous step



# Test codes
Run the following command on a machine with docker( Just `base/tests` has been implemented )

```bash
docker run --rm time-tracking-django-api ./entry.sh test
```


# Model Architecture
We have the following Models:

1. User
2. Project: which has a creator so it has a `ForeignKey` to `User`
3. TimeLog: which has an owner and also a related project so it has a `ForeignKey` to `User` and `Project`

![models diagram](Diagram.png "models diagram")


# Decision has been made

1. All `on_delete` for `ForeignKey`s are set to `RESTRICT` as it's make sense to keep `Project`s and `TimeLog`s for the future decistion as it is a time tracking system and even if a user or project delete in the future, it's still reasonable to keep time log and it will help for the future estimation or even for proofing something in the future. It's a good idea to implement soft delete (I did not implement soft delete for this project) instead of really deleting the objects.
2. For `TimeLog` I used `duration` field instead of `end_time`. The reason was it will improve for quries like sorting time logs according to duration or calculating total time for a project. It's still make sense to also add `end_time` if we have some quries that need this data and it will improve that kind of queries despite it is a redundant data.
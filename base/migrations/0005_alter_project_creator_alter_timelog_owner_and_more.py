# Generated by Django 4.1 on 2022-08-19 00:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("base", "0004_alter_timelog_owner"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="creator",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="projects",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="timelog",
            name="owner",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="time_logs",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="timelog",
            name="project",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="time_logs",
                to="base.project",
            ),
        ),
    ]

import uuid

from django.db import models
from django.db.models import RESTRICT
from django.contrib.auth.models import User


class Project(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=256)
    creator = models.ForeignKey(User, on_delete=RESTRICT, related_name="projects")

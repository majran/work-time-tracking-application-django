import uuid

from django.db import models
from django.db.models import RESTRICT
from django.contrib.auth.models import User

from base.models.project import Project


class TimeLog(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    start_time = models.DateTimeField()
    duration = models.DurationField()
    project = models.ForeignKey(Project, on_delete=RESTRICT, related_name="time_logs")
    owner = models.ForeignKey(User, on_delete=RESTRICT, related_name="time_logs")

from rest_framework import serializers

from base.models.time_log import TimeLog
from base.serializers.project import ProjectListSerializer


class TimeLogListSerializer(serializers.ModelSerializer):
    project = ProjectListSerializer()

    class Meta:
        model = TimeLog
        fields = "__all__"


class TimeLogCreateSerializer(serializers.ModelSerializer):
    project = serializers.PrimaryKeyRelatedField(
        read_only=True, pk_field=serializers.UUIDField(format="hex_verbose")
    )

    class Meta:
        model = TimeLog
        fields = ["start_time", "duration", "project"]


class TimeLogUpdateSerializer(serializers.ModelSerializer):
    project = serializers.PrimaryKeyRelatedField(
        read_only=True, pk_field=serializers.UUIDField(format="hex_verbose")
    )

    class Meta:
        model = TimeLog
        fields = ["start_time", "duration", "project"]
        extra_kwargs = {
            "start_time": {"required": False},
            "duration": {"required": False},
        }

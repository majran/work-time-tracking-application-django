from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth.models import User

from base.models.project import Project


class APITests(APITestCase):
    USER_DATA = {
        "username": "username_test",
        "password": "maypass123",
        "password2": "maypass123",
        "email": "email@egmail.com",
        "first_name": "first_name",
        "last_name": "last_name",
    }
    TOKEN = None

    def TestCreate_user(self):
        """
        Ensure we can create a new user object.
        """
        url = reverse("auth_register")

        response = self.client.post(url, self.USER_DATA, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, "username_test")

    def TestLogin_user(self):
        """
        Ensure a user can login.
        """
        url = reverse("token_obtain_pair")
        USER_LOGIN_DATA = {
            "username": self.USER_DATA["username"],
            "password": self.USER_DATA["password"],
        }
        response = self.client.post(url, USER_LOGIN_DATA, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.TOKEN = response.data["access"]

    def TestCreate_project(self):
        """
        Ensure we can create a new project object.
        """
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Bearer " + self.TOKEN)
        url = reverse("project-list")
        PROJECT_DATA = {"title": "project_test"}
        response = client.post(url, PROJECT_DATA, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Project.objects.count(), 1)
        self.assertEqual(Project.objects.get().title, "project_test")

    def test(self):
        self.TestCreate_user()
        self.TestLogin_user()
        self.TestCreate_project()

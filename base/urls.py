from rest_framework.routers import DefaultRouter

from base.views.project import ProjectViewSet
from base.views.time_log import TimeLogViewSet


urlpatterns = []

router = DefaultRouter()
router.register("project", ProjectViewSet, basename="project")
router.register("time_log", TimeLogViewSet, basename="time_log")

urlpatterns += router.urls

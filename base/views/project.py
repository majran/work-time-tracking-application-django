from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from base.serializers.project import (
    ProjectListSerializer,
    ProjectCreateSerializer,
    ProjectUpdateSerializer,
)
from base.models.project import Project


class ProjectViewSet(ModelViewSet):
    serializer_class = ProjectListSerializer
    permission_classes = (IsAuthenticated,)
    http_method_names = ["get", "post", "put", "delete"]
    queryset = Project.objects.all()

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return self.serializer_class
        elif self.action == "update":
            return ProjectUpdateSerializer
        elif self.action == "create":
            return ProjectCreateSerializer
        else:
            return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from base.serializers.time_log import (
    TimeLogListSerializer,
    TimeLogCreateSerializer,
    TimeLogUpdateSerializer,
)
from base.models.time_log import TimeLog
from base.models.project import Project


class TimeLogViewSet(ModelViewSet):
    serializer_class = TimeLogListSerializer
    permission_classes = (IsAuthenticated,)
    http_method_names = ["get", "post", "put", "delete"]
    queryset = TimeLog.objects.all()

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return self.serializer_class
        elif self.action == "update":
            return TimeLogUpdateSerializer
        elif self.action == "create":
            return TimeLogCreateSerializer
        else:
            return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(
            owner=self.request.user,
            project=Project.objects.get(id=self.request.data["project"]),
        )
